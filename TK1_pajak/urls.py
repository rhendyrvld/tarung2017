from django.urls import path
from . import views
from .views import add_pajak

urlpatterns = [
    path('', views.add_pajak, name='add_pajak'),
]

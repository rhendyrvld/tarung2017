from django.apps import AppConfig


class Tk1PajakConfig(AppConfig):
    name = 'TK1_pajak'

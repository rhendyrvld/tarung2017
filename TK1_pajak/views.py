from django.shortcuts import render, HttpResponseRedirect
from .forms import Add_Pajak
from .models import Pajak

# Create your views here.
def add_pajak(request):
    form = Add_Pajak(request.POST or None)
    response = {'form_pajak' : form}
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['ktp'] = request.POST['ktp']
        response['address'] = request.POST['address']
        response['handphone'] = request.POST['handphone']
        response['vehicle_type'] = request.POST['vehicle_type']
        new_pajak = Pajak(name=response['name'], ktp=response['ktp'], address=response['address'], handphone=response['handphone'], vehicle_type=response['vehicle_type'])
        new_pajak.save()
    return render(request, 'bayar_pajak.html', response)
from django.apps import AppConfig


class Tk1HomeConfig(AppConfig):
    name = 'TK1_home'

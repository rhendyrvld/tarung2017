from django.contrib import admin
from django.urls import *
from .views import *

app_name='TK1_home'
urlpatterns = [
    path('', index, name='index'),
]
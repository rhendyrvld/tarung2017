from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime, date
# from .forms import *
# from .models import *
from django.urls import reverse

# Create your views here.
def index(request):
	response = {}
	return render(request, 'index.html', response)
from django.shortcuts import render, HttpResponseRedirect
from .forms import Add_Sim
from .models import Sim

# Create your views here.
def add_sim(request):
    form = Add_Sim(request.POST or None)
    response = {'form_sim' : form}
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['ktp'] = request.POST['ktp']
        response['address'] = request.POST['address']
        response['handphone'] = request.POST['handphone']
        response['sim_type'] = request.POST['sim_type']
        new_sim = Sim(name=response['name'], ktp=response['ktp'], address=response['address'], handphone=response['handphone'], sim_type=response['sim_type'])
        new_sim.save()
    return render(request, 'add_sim.html', response)


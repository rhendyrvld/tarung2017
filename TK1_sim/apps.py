from django.apps import AppConfig


class Tk1SimConfig(AppConfig):
    name = 'TK1_sim'

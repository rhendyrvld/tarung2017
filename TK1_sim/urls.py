from django.urls import path
from . import views
from .views import add_sim

urlpatterns = [
    path('', views.add_sim, name='add_sim'),
]

from django.apps import AppConfig


class Tk1FaqConfig(AppConfig):
    name = 'TK1_faq'

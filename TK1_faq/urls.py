from django.contrib import admin
from django.urls import *
from . import views

app_name='TK1_faq'
urlpatterns = [
    path('', views.index, name='faq-index'),
]
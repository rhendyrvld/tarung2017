from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from datetime import datetime, date
from django.urls import reverse
from .models import Question
from .forms import QuestionForm

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path_info)
        else:
            return HttpResponseRedirect('')
    else:
        form = QuestionForm()
        return render(request, 'faq-index.html', {'form' : form})

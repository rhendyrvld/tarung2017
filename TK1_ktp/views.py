from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import KTP
from .forms import KTPForms


def index(request) :
    form = KTPForms(request.POST)
    response = {'form' : KTPForms()}
    if(request.method == 'POST' and form.is_valid()):
        name = form.cleaned_data['name']
        gender = form.cleaned_data['gender']
        address = form.cleaned_data['address']
        province = form.cleaned_data['province']
        kecamatan = form.cleaned_data['kecamatan']
        kelurahan = form.cleaned_data['kelurahan']
        date_birth = form.cleaned_data['date_birth']
        date_place = form.cleaned_data['date_place']
        agama = form.cleaned_data['agama']
        warga = form.cleaned_data['warga']
        pekerjaan = form.cleaned_data['pekerjaan']
        p = KTP(name=name, gender=gender, address=address,
        province=province, kecamatan=kecamatan, kelurahan=kelurahan,
        date_birth=date_birth,date_place=date_place, agama=agama,
        pekerjaan=pekerjaan)
        p.save()
        return HttpResponseRedirect(reverse("TK1_ktp:hasil"))
    else:
        return render(request, 'ktp.html', response)

def hasil(request) :
    jadwal = KTP.objects.all()
    response = {'tabel' : jadwal}
    return render(request, 'hasil.html', response)

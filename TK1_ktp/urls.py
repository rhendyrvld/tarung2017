from django.contrib import admin
from django.urls import *
from .views import index,hasil

app_name='TK1_ktp'
urlpatterns = [
    path('ktp/', index, name='ktp'),
    path('hasil/',hasil, name='hasil'),

]

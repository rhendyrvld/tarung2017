from django import forms
from django.forms import ModelForm
import datetime

GENDER_CHOICES = [
    ('pria', 'Pria'),
    ('wanita', 'Wanita'),
]

RELIGION_CHOICES = [
    ('islam', 'Islam'),
    ('kristen', 'Kristen'),
    ('katolik', 'Katolik'),
    ('budha', 'Budha'),
    ('kong hu chu', 'Kong Hu Chu'),
]

WARGA_CHOICES = [
    ('wni', 'WNI'),
    ('ln', 'Luar Negeri'),
]

class KTPForms(forms.Form):

    name = forms.CharField(label='Nama', max_length=100)
    gender = forms.CharField(label='Jenis Kelamin',widget=forms.Select(choices=GENDER_CHOICES))
    address = forms.CharField(label = 'Alamat', max_length = 1000)
    province = forms.CharField(label= 'Provinsi')
    kecamatan= forms.CharField(label = 'Kecamatan')
    kelurahan = forms.CharField(label = 'Kelurahan')
    date_birth = forms.DateTimeField(label = "Tanggal lahir", initial=datetime.date.today)
    date_place = forms.CharField(label="Tempat Lahir", max_length=20)
    agama = forms.CharField(label="Agama",widget=forms.Select(choices=RELIGION_CHOICES))
    warga = forms.CharField(label="Kewarnegaraan",widget=forms.Select(choices=WARGA_CHOICES))
    pekerjaan = forms.CharField(label="Pekerjaan", max_length=20)

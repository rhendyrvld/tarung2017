from django.db import models
from django.forms import ModelForm

class KTP(models.Model) :
    PRIA = 'pria'
    WANITA = 'wanita'
    GENDER_CHOICES = [
        (PRIA, 'pria'),
        (WANITA, 'wanita'),
    ]

    RELIGION_CHOICES = [
        ('islam', 'Islam'),
        ('kristen', 'Kristen'),
        ('katolik', 'Katolik'),
        ('budha', 'Budha'),
        ('kong hu chu', 'Kong Hu Chu'),
    ]

    WARGA_CHOICES = [
        ('wni', 'WNI'),
        ('ln', 'Luar Negeri'),
    ]

    name = models.CharField(max_length = 100)
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES)
    address = models.CharField(max_length = 200)
    #better to make choices for province, kecamatan, and Kelurahan
    #but it's not feasible
    province = models.CharField(max_length=20)
    kecamatan = models.CharField(max_length=20)
    kelurahan = models.CharField(max_length=20)
    date_birth = models.DateField()
    date_place = models.CharField(max_length=50)
    agama = models.CharField(max_length=11, choices=RELIGION_CHOICES)
    warga = models.CharField(max_length=10, choices=WARGA_CHOICES)
    pekerjaan = models.CharField(max_length=20)

    def __str__(self):
        return self.name
